<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Belanja extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('status')==TRUE) 
        {
            $this->load->model('M_admin');
        }else{  
            redirect('C_login');
        }
    }
    

    public function index()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/sidebar');
        $this->load->view('admin/v_belanja');
        $this->load->view('admin/footer');
    }
}